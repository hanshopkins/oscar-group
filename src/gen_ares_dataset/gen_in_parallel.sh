#!/bin/bash

rm -rf $scratch/gad_dTb/*
rm -rf $scratch/gad_xHII/*
rm -rf $scratch/gad_params/*
cd ~
. use_ares.sh
cd oscar-group/src/gen_ares_dataset
for i in 1 2 3
do
	mpirun -n 40 python gen_ares_dataset.py
	python combine.py
	echo "loop $i done"
done
