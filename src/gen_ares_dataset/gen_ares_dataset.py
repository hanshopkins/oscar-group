import numpy as np
import ares
from scipy.interpolate import CubicSpline
import time
import os

ares_params = [["pop_Tmin_0_", 1E3, 1.25E5, "log"],
["pop_fesc_0_", 0.108, 0.358, "lin"],
["pop_rad_yield_1_", 5E38, 5E40, "log"],
["pq_func_par2[0]_0_", 0.378, 0.6605, "lin"], #gamma_lo
["pq_func_par0[0]_0_", 0.0271, 0.0892, "log"]] #f_star,p

# function to set all parameters into the [param_min, param_max] range
def denormalize (arr):
	op = np.empty(arr.shape)
	for i in range(arr.shape[1]):
		if ares_params[i][3] == "lin":
			op[:,i] = arr[:,i] * (ares_params[i][2] - ares_params[i][1]) + ares_params[i][1]
		elif ares_params[i][3] == "log":
			op[:,i] = 10.0**(arr[:,i] * (np.log10(ares_params[i][2]) - np.log10(ares_params[i][1])) + np.log10(ares_params[i][1]))
		else:
			raise ValueError("Invalid normalization type in ares_params")
	return op

redshifts = np.linspace(5,30,40)

num_params = len(ares_params)

def call_ares_from_list (param_list):
	input_dict = {}
	for i in range(len(ares_params)):
		input_dict[ares_params[i][0]] = param_list[i]

	tmp=ares.util.ParameterBundle('mirocha2017:base')
	tmp.update(input_dict)
	tmp.update({"pop_binaries_0_":True})
	params=tmp
	simu = ares.simulations.Global21cm(**params, verbose=False, progress_bar=False)
	simu.run()  
	z = simu.history['z'][::-1]
	dTb = simu.history['dTb'][::-1]
	cgm_e=simu.history['cgm_e'][::-1]
	sorted_idx = np.argsort(z,kind="stable")
	z = z[sorted_idx]
	dTb = dTb[sorted_idx]
	xHII = cgm_e[sorted_idx]
	splinedTb = CubicSpline(z, dTb)
	splinexHII = CubicSpline(z, xHII)
	return splinedTb(redshifts), splinexHII(redshifts)

for i in range(30):
	#randomly choosing output values
	params_array = denormalize(np.random.rand(len(ares_params))[np.newaxis])[0]
	dTb_array, xHII_array = call_ares_from_list(params_array)

	#saving the results
	pid = os.getpid()
	os.chdir("/scratch/s/sievers/hans1")
	try:
		os.mkdir("gad_dTb") #stands for gen ares dataset
		os.mkdir("gad_xHII") #stands for gen ares dataset
		os.mkdir("gad_params")
	except:
		pass

	try:
		existing_dTb = np.loadtxt("gad_dTb/dTb"+str(pid)+".gz").reshape([-1, redshifts.shape[0]]) #I have to reshape so that it's not a 1d array
		existing_xHII = np.loadtxt("gad_xHII/xHII"+str(pid)+".gz").reshape([-1, redshifts.shape[0]])
		existing_params = np.loadtxt("gad_params/params"+str(pid)+".gz").reshape([-1, num_params])
		
		dTb_to_save = np.append(existing_dTb, dTb_array[np.newaxis], axis = 0)
		xHII_to_save = np.append(existing_xHII, xHII_array[np.newaxis], axis = 0)
		params_to_save = np.append(existing_params, params_array[np.newaxis], axis = 0)
		
		np.savetxt("gad_dTb/dTb"+str(pid)+".gz", dTb_to_save)
		np.savetxt("gad_xHII/xHII"+str(pid)+".gz", xHII_to_save)
		np.savetxt("gad_params/params"+str(pid)+".gz", params_to_save)
	except OSError: #when file not found
		np.savetxt("gad_dTb/dTb"+str(pid)+".gz", dTb_array)
		np.savetxt("gad_xHII/xHII"+str(pid)+".gz", xHII_array)
		np.savetxt("gad_params/params"+str(pid)+".gz", params_array)
