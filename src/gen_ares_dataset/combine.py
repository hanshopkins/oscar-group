import os
import numpy as np

os.chdir("/scratch/s/sievers/hans1")

tfiles_dTb = os.listdir("gad_dTb")

try:
	add = np.loadtxt("ares_dataset_dTb.gz")
	adx = np.loadtxt("ares_dataset_xHII.gz")
	adp = np.loadtxt("ares_dataset_params.gz")
except OSError:
	add = np.empty((0, 40)) #change the size in this line
	adx = np.empty((0, 40))
	adp = np.empty((0, 5)) #change the size in this line
	
for name in tfiles_dTb:
	pid = name[3:-3]
	add = np.append(add, np.loadtxt("gad_dTb/"+name), axis = 0)
	adx = np.append(adx, np.loadtxt("gad_xHII/xHII"+str(pid)+".gz"), axis = 0)
	adp = np.append(adp, np.loadtxt("gad_params/params"+str(pid)+".gz"), axis = 0)
	os.remove("gad_dTb/"+name)
	os.remove("gad_xHII/xHII"+str(pid)+".gz")
	os.remove("gad_params/params"+pid+".gz")

np.savetxt("ares_dataset_dTb.gz", add)
np.savetxt("ares_dataset_xHII.gz", adx)
np.savetxt("ares_dataset_params.gz", adp)
