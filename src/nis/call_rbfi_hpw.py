import numpy as np
from scipy.interpolate import RBFInterpolator
from ares_params import redshifts

def call_rbfi(rbfi_list, params):
	op = np.empty(len(rbfi_list))
	for i in range(len(rbfi_list)):
		op[i] = rbfi_list[i](params)
	return op

def call_rbfi_many(rbfi_list, params):
	# the same function but expecting a list of params
	# returns an array of curve values
	op = np.empty([params.shape[0],len(rbfi_list)])
	for i in range(len(rbfi_list)):
		op[:,i] = rbfi_list[i](params)
	return op
