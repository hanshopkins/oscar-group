import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import time
import neatnik
from call_rbfi_t2 import call_rbfi_many
from curve_details import curve_details
from ares_params import redshifts, normalize

testCurves = np.loadtxt(os.environ["PROJECT"]+"/nis/test_curves.gz")
testParams = normalize(np.loadtxt(os.environ["PROJECT"]+"/nis/test_params.gz"))
testDetails = curve_details(testCurves)

os.chdir("/project/s/sievers/hans1/nis/t2_rbfis")
rbfilist = []
for i in range(len(os.listdir())):
	with open("rbfi"+str(i)+".pickle", "rb") as file:
		rbfilist.append(pickle.load(file))

t1 = time.time()
neural_network = neatnik.Organism(pickle.load(open(os.environ["PROJECT"]+"/nis/organism_hpw.p", "rb")))
t2 = time.time()
print("Time difference for calculating", str(testCurves.shape[0]),"curves is", str(t2-t1))
print("Average time difference is", str((t2-t1)/testCurves.shape[0]))

if __name__ == "__main__":
	reactions = np.asarray(neural_network.react([curve_details(testCurves)])[0])
	true_from_rbfi = call_rbfi_many(rbfilist, testParams)
	nn_curves = call_rbfi_many(rbfilist, reactions)
	nn_details = curve_details(nn_curves)

	relative_errors = np.trapz(np.abs(1.0-nn_curves/true_from_rbfi), redshifts, axis = 1)*100/(redshifts[-1]-redshifts[0])

	os.chdir(os.environ["PROJECT"]+"/nis")
	
	if True:
		#splitting them up into low error and high error
		lowidx = np.nonzero(relative_errors <= 200)
		highidx = np.nonzero(relative_errors > 200)
		fig1 = plt.figure()
		plt.hist(relative_errors[lowidx], bins = 100)
		plt.xlabel("Relative Error (%)")
		plt.title("Histogram of low Neatnik relative error curves")
		plt.savefig("outputs/nn_rel_error_hist")
        
		fig2 = plt.figure()
		plt.hist(np.abs((nn_details[:,0]-testDetails[:,0])/testDetails[:,0]), bins = 100)
		plt.xlabel("Relative Error")
		plt.title("Relative error of heights")
		plt.savefig("outputs/heights_err_hist")

		fig3 = plt.figure()
		plt.hist(np.abs((nn_details[:,1]-testDetails[:,1])/testDetails[:,1]), bins = 100)
		plt.xlabel("Relative Error")
		plt.title("Relative error of trough positions")
		plt.savefig("outputs/pos_err_hist")

		fig4 = plt.figure()
		plt.hist(np.abs((nn_details[:,2]-testDetails[:,2])/testDetails[:,2]), bins = 100)
		plt.xlabel("Relative Error")
		plt.title("Relative error of trough widths")
		plt.savefig("outputs/width_err_hist")
