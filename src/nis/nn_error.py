import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import time
import neatnik
from call_rbfi_t2 import call_rbfi_many
from ares_params import redshifts, normalize

testCurves = np.loadtxt(os.environ["PROJECT"]+"/nis/test_curves.gz")
testParams = normalize(np.loadtxt(os.environ["PROJECT"]+"/nis/test_params.gz"))

os.chdir(os.environ["PROJECT"]+"/nis/t2_rbfis")
rbfilist = []
for i in range(len(os.listdir())):
	with open("rbfi"+str(i)+".pickle", "rb") as file:
		rbfilist.append(pickle.load(file))

t1 = time.time()
neural_network = neatnik.Organism(pickle.load(open(os.environ["PROJECT"]+"/nis/organism.p", "rb")))
t2 = time.time()
print("Time difference for calculating", str(testCurves.shape[0]),"curves is", str(t2-t1))
print("Average time difference is", str((t2-t1)/testCurves.shape[0]))

if __name__ == "__main__":
	reactions = np.asarray(neural_network.react([testCurves])[0])
	true_from_rbfi = call_rbfi_many(rbfilist, testParams)
	nn_curves = call_rbfi_many(rbfilist, reactions)

	abs_errors = np.trapz(np.abs(true_from_rbfi - nn_curves), axis = 1)/(redshifts[-1]-redshifts[0])
	relative_errors = np.trapz(np.abs(1.0-nn_curves/true_from_rbfi), redshifts, axis = 1)*100/(redshifts[-1]-redshifts[0])

	#calculating the cumulative means
	cum_mean_abs = np.cumsum(abs_errors)/(np.arange(abs_errors.shape[0])+1)
	cum_mean_rel = np.cumsum(relative_errors)/(np.arange(relative_errors.shape[0])+1)

	os.chdir(os.environ["PROJECT"]+"/nis")
	
	if False:
		fig1 = plt.figure()
		plt.plot(cum_mean_abs)
		plt.xlabel("Index")
		plt.ylabel("Cumulative absolute error (mK)")
		plt.title("Checking abs error convergence for Neatnik")
		plt.yticks(np.arange(0,np.max(cum_mean_abs),1))
		plt.savefig("outputs/nn_abs_error")

		fig2 = plt.figure()
		plt.plot(cum_mean_rel)
		plt.yscale("log")
		plt.xlabel("Index")
		plt.ylabel("Cumulative relative error")
		plt.title("Checking relative error convergence for Neatnik")
		plt.savefig("outputs/nn_rel_error")

		fig3 = plt.figure()
		plt.hist(abs_errors, 40)
		plt.xlabel("Abs Error (mK)")
		plt.title("Abs error histogram for Neatnik")
		plt.savefig("outputs/nn_abs_error_hist")

		fig4 = plt.figure()
		#plt.hist(relative_errors, bins = np.logspace(np.log10(np.min(relative_errors)), np.log10(np.max(relative_errors)), num=40))
		plt.hist(relative_errors, bins = 40)
		plt.xlabel("Relative Error")
		plt.title("Relative error histogram for Neatnik")
		plt.savefig("outputs/nn_rel_error_hist")

	if True:
		#splitting them up into low error and high error
		lowidx = np.nonzero(relative_errors <= 400)
		highidx = np.nonzero(relative_errors > 400)
		fig4 = plt.figure()
		#plt.hist(relative_errors, bins = np.logspace(np.log10(np.min(relative_errors)), np.log10(np.max(relative_errors)), num=40))
		plt.hist(relative_errors[lowidx], bins = 100)
		plt.xlabel("Relative Error")
		plt.title("Histogram of low Neatnik relative error curves")
		plt.savefig("outputs/nn_low_rel_error_hist")

		fig5 = plt.figure()
		#plt.hist(relative_errors, bins = np.logspace(np.log10(np.min(relative_errors)), np.log10(np.max(relative_errors)), num=40))
		plt.hist(relative_errors[highidx], bins = 40)
		plt.xlabel("Relative Error")
		plt.title("Histogram of high Neatnik relative error curves")
		plt.savefig("outputs/nn_high_rel_error_hist")
