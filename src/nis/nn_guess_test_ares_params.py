import numpy as np
import neatnik
import pickle as p
from ares_params import denormalize

graph = p.load(open(os.environ["PROJECT"]+"/nis/organism.p", "rb"))

neural_network = neatnik.Organism(graph)

testCurves = np.loadtxt(os.environ["PROJECT"]+"/nis/test_curves.gz")[:5]

reactions = np.asarray(neural_network.react([testCurves])[0])

denormalized_reactions = denormalize(reactions)

np.savetxt(os.environ["PROJECT"]+"/nis/nn_testOP.txt",denormalized_reactions)
