import numpy as np
import neatnik
import pickle as p
from ares_params import denormalize
graph = p.load(open("/project/s/sievers/hans1/nis/organism.p", "rb"))
neural_network = neatnik.Organism(graph)
edges_curve = np.loadtxt("/project/s/sievers/hans1/nis/edges_data.txt")*500
reactions = np.asarray(neural_network.react([[edges_curve]])[0])
denormalized_reactions = denormalize(reactions)
print(denormalized_reactions)
print(reactions)
