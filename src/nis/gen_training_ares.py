import numpy as np
import ares
from multiprocessing import Pool
from call_ares import call_ares
from ares_params import ares_params, redshifts, denormalize
import time

N = 800 #number of training points without the added corner points
num_params = len(ares_params)

inputs_array = np.empty([N + 2**num_params,redshifts.shape[0]])

outputs_array = np.empty([N+2**num_params,len(ares_params)])

#randomly choosing output values
outputs_array[0:N] = np.random.rand(N,len(ares_params)) #normalized until later in the script

num_params = len(ares_params)

#adding all the corners
def gcv (n):
    p = np.zeros([2**n,n])
    for i in range(2**n):
        num = i
        for j in range(n):
            if num >= 2**(n-j-1):
                num -= 2**(n-j-1)
                p[i][j] = 1
    return p

outputs_array[-2**num_params:] = gcv(num_params)
outputs_array = denormalize(outputs_array) #denormalizing outputs array

def call_ares_from_list (param_list):
	input_dict = {}
	for i in range(len(ares_params)):
		input_dict[ares_params[i][0]] = param_list[i]
	return call_ares(input_dict, redshifts)

t1 = time.time()
with Pool(40) as p:
	inputs_array = np.asarray(p.map(call_ares_from_list, outputs_array.tolist()))

print("Total time is", time.time()-t1)
print("Time per point:", (time.time()-t1)/inputs_array.shape[0])

np.savetxt("/project/s/sievers/hans1/nis/ares_training_inputs.gz", inputs_array)
np.savetxt("/project/s/sievers/hans1/nis/ares_training_outputs.gz", outputs_array)

#generating five test curves
test_params = denormalize(np.random.rand(5,len(ares_params)))
test_curves = np.empty([5,redshifts.shape[0]])
with Pool(40) as p:
	test_curves = np.asarray(p.map(call_ares_from_list, test_params.tolist()))


np.savetxt("/project/s/sievers/hans1/nis/ares_test_params.gz", test_params)
np.savetxt("/project/s/sievers/hans1/nis/ares_test_curves.gz", test_curves)
