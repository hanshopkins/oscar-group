import numpy as np
from curve_details import curve_details
from ares_params import redshifts, normalize
from scipy.interpolate import RBFInterpolator
import os
import pickle

if __name__ == "__main__":
	curves = np.loadtxt(os.environ["PROJECT"]+"/nis/interp_curves.gz")
	Param = normalize(np.loadtxt(os.environ["PROJECT"]+"/nis/interp_params.gz"))	

	rbfilist = []
	for i in range(redshifts.shape[0]):
		rbfilist.append(RBFInterpolator(Param, curves[:,i], kernel = "inverse_quadratic", epsilon = 1))

	#saving the rbf interpolators
	os.chdir(os.environ["PROJECT"]+"/nis")
	if not "t2_rbfis" in os.listdir():
		os.mkdir("t2_rbfis")
	os.chdir("t2_rbfis")
	for i in range(len(rbfilist)):
		with open("rbfi"+str(i)+".pickle", "wb") as f:
			pickle.dump(rbfilist[i], f)
