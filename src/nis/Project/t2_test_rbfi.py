import numpy as np
import matplotlib.pyplot as plt
import pickle
import sys
import os
import time
sys.path.append("/home/s/sievers/hans1/oscar-group/src/nis")
from call_rbfi_t2 import call_rbfi_many
from ares_params import redshifts, normalize

testCurves = np.loadtxt(os.environ["PROJECT"]+"/nis/test_curves.gz")
testParams = normalize(np.loadtxt(os.environ["PROJECT"]+"/nis/test_params.gz"))

os.chdir("/project/s/sievers/hans1/nis/t2_rbfis")
rbfilist = []
for i in range(len(os.listdir())):
	with open("rbfi"+str(i)+".pickle", "rb") as file:
		rbfilist.append(pickle.load(file))

def center_bins(bin_edges):
	avgs = (bin_edges + np.roll(bin_edges,-1))/2
	return avgs[:-1]

if __name__ == "__main__":
	t1 = time.time()
	rbfiCurves = call_rbfi_many(rbfilist, testParams)
	print("Average time to get curve from interpolator:", (time.time()-t1)/testParams.shape[0])

	abs_errors = np.sum(np.abs(rbfiCurves - testCurves), axis = 1)
	relative_errors = np.trapz(np.abs(1.0-rbfiCurves/testCurves), redshifts, axis = 1)*100/(redshifts[-1]-redshifts[0])
	print("mean relative error", np.mean(relative_errors))

	#calculating the cumulative means
	cum_mean_abs = np.cumsum(abs_errors)/(np.arange(abs_errors.shape[0])+1)
	cum_mean_rel = np.cumsum(relative_errors)/(np.arange(relative_errors.shape[0])+1)

	os.chdir("/project/s/sievers/hans1/nis")
	
	if False:
		fig1 = plt.figure()
		plt.plot(cum_mean_abs)
		plt.xlabel("Index")
		plt.ylabel("Cumulative absolute error (mK)")
		plt.title("Checking abs error convergence for t2 rbfi")
		plt.yticks(np.arange(0,np.max(cum_mean_abs),1))
		plt.savefig("outputs/abs_error_t2")

		fig2 = plt.figure()
		plt.plot(cum_mean_rel)
		plt.yscale("log")
		plt.xlabel("Index")
		plt.ylabel("Cumulative relative error")
		plt.title("Checking relative error convergence for t2 rbfi")
		plt.savefig("outputs/rel_error_t2")

		fig3 = plt.figure()
		plt.hist(abs_errors, 40)
		plt.xlabel("Abs Error (mK)")
		plt.title("Abs error histogram for t2 rbfi")
		plt.savefig("outputs/abs_error_hist_t2")

		fig4 = plt.figure()
		#plt.hist(relative_errors, bins = np.logspace(np.log10(np.min(relative_errors)), np.log10(np.max(relative_errors)), num=40))
		plt.hist(relative_errors, bins = 40)
		plt.xlabel("Relative Error")
		plt.title("Relative error histogram for t2 rbfi (100 neighbors)")
		plt.savefig("outputs/rel_error_hist_t2")


	if False:
		bad_rel_error = np.argwhere(relative_errors > 1000)
		idx = bad_rel_error[0][0]
		fig5 = plt.figure()
		plt.plot(redshifts, testCurves[idx], "b-", label = "true curve")
		plt.plot(redshifts, rbfiCurves[idx], "r", label = "rbfi approx")
		plt.xlabel("redshift")
		plt.ylabel(r"\delta T_b")
		plt.title("Curve with relative error " + str(relative_errors[idx]))
		plt.legend()
		plt.savefig("outputs/high_rel_error_curve")

		fig6 = plt.figure()
		plt.plot(redshifts[8:-8], np.abs((rbfiCurves[idx]-testCurves[idx])/testCurves[idx])[8:-8])
		plt.xlabel("redshift")
		plt.ylabel("relative error")
		plt.title("Relative error in non zero region")
		plt.savefig("outputs/hre_curve_cer")

		fig7 = plt.figure()
		plt.plot(redshifts[8:-8], np.abs(rbfiCurves[idx]-testCurves[idx])[8:-8])
		plt.xlabel("redshift")
		plt.ylabel("Absolute error (mK)")
		plt.title("Absolute error in non zero region")
		plt.savefig("outputs/hre_curve_cea")
	if False:
		#splitting them up into low error and high error
		lowidx = np.nonzero(relative_errors <= 50)
		highidx = np.nonzero(relative_errors > 50)
		fig4 = plt.figure()
		#plt.hist(relative_errors, bins = np.logspace(np.log10(np.min(relative_errors)), np.log10(np.max(relative_errors)), num=40))
		plt.hist(relative_errors[lowidx], bins = 100)
		plt.xlabel("Relative Error")
		plt.title("Histogram of low rbfi relative error curves")
		plt.savefig("outputs/low_rel_error_hist_t2")

		fig5 = plt.figure()
		#plt.hist(relative_errors, bins = np.logspace(np.log10(np.min(relative_errors)), np.log10(np.max(relative_errors)), num=40))
		plt.hist(relative_errors[highidx], bins = 40)
		plt.xlabel("Relative Error")
		plt.title("Histogram of high rbfi relative error curves")
		plt.savefig("outputs/high_rel_error_hist_t2")
