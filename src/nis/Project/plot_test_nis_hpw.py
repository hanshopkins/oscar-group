import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append("/home/s/sievers/hans1/oscar-group/src/nis")
sys.path.append("/home/s/sievers/hans1/ares/scripts")
from call_ares import call_ares
from ares_params import ares_params, redshifts, denormalize
from curve_details import curve_details

testCurves = np.loadtxt("test_curves.gz")[:5]
testcd = curve_details(testCurves)
nn_testOP = np.loadtxt("nn_testOP.txt")

def call_ares_array (p_array):
	input_dict = {}
	for j in range(len(ares_params)):
		input_dict[ares_params[j][0]] = p_array[j]
	return call_ares(input_dict, redshifts)

nn_testOP_curves = np.empty([5,redshifts.shape[0]])
for i in range(5):
	nn_testOP_curves[i] = call_ares_array(nn_testOP[i])

nn_testOP_details = curve_details(nn_testOP_curves)

if __name__ == "__main__":
	plt.plot(redshifts, testCurves[0], color="r", ls=":", label = "True curve")
	plt.plot(redshifts, testCurves[1], color="g", ls=":")
	plt.plot(redshifts, testCurves[2], color="b", ls=":")
	plt.plot(redshifts, testCurves[3], color="c", ls=":")
	plt.plot(redshifts, testCurves[4], color="m", ls=":")
	plt.plot(redshifts, nn_testOP_curves[0], color="r", ls="-", label = "Neatnik guess")
	plt.plot(redshifts, nn_testOP_curves[1], color="g", ls="-")
	plt.plot(redshifts, nn_testOP_curves[2], color="b", ls="-")
	plt.plot(redshifts, nn_testOP_curves[3], color="c", ls="-")
	plt.plot(redshifts, nn_testOP_curves[4], color="m", ls="-")
	plt.xlabel(r"$z$")
	plt.ylabel(r"$\delta T_b$ (mK)")
	plt.title("Comparing true test curves to the hpw neural network's output")
	plt.legend()
	plt.savefig("outputs/testplot.pdf")

	#print("True curve details:")
	#print(testcd)
	#print("Neatnik curve details:")
	#print(nn_testOP_details)
	#print("Maximum height difference:", np.max(np.abs(testcd[:,0]-nn_testOP_details[:,0])))
	#print("Average height difference:", np.mean(np.abs(testcd[:,0]- nn_testOP_details[:,0])))
	#print("Maximum position difference:", np.max(np.abs(testcd[:,1]- nn_testOP_details[:,1])))
	#print("Average position difference:", np.mean(np.abs(testcd[:,1]- nn_testOP_details[:,1])))
	#print("Maximum width difference:", np.max(np.abs(testcd[:,2]- nn_testOP_details[:,2])))
	#print("Average width difference:", np.mean(np.abs(testcd[:,2]- nn_testOP_details[:,2])))
