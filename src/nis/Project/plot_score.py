import numpy as np
import matplotlib.pyplot as plt
import sys

display_score = np.loadtxt("display_score.gz")

if "skip_burnin" not in sys.argv:
	plt.plot(np.arange(display_score.shape[0]), display_score, linestyle = "", marker = ".", label = "Best score", markersize = 3)
	plt.xlabel("Generation")
	plt.yscale("log")
	plt.ylabel("Score")
	plt.title("Checking score convergence")
	plt.savefig("outputs/score_convergence")
else:
	start_idx = int(display_score.shape[0]*0.1)
	plt.plot(np.arange(start_idx,display_score.shape[0]), display_score[start_idx:], linestyle = "", marker = ".", label = "Best score", markersize = 3)
	plt.yscale("log")
	plt.xlabel("Generation")
	plt.ylabel("Score")
	plt.title("Checking score convergence")
	plt.savefig("outputs/score_convergence")

