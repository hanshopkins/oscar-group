#import
import neatnik
import parameters

# Typing:
from neatnik import Experiment
from neatnik import Organism

# Others:
import numpy as np
import pickle
from ares_params import ares_params, normalize, denormalize
from call_rbfi_hpw import call_rbfi_many
from curve_details import curve_details
import os
import time

#loading the rbfi
os.chdir(os.environ["PROJECT"]+"/nis/hpw_rbfis")
rbfilist = []
for i in range(len(os.listdir())):
	with open("rbfi"+str(i)+".pickle", "rb") as file:
		rbfilist.append(pickle.load(file))

display_score = np.empty(neatnik.Parameters.generational_cycles+1)

class inv_ares(Experiment):
    """ Find parameters from an ARES sim """

    def __init__(self) -> None:
        """ Initializes experiment """

        super().__init__()
	
        number_of_inputs = 1

        vertexes_list = []
        for i in range(number_of_inputs):
            vertexes_list.append((i, None, neatnik.ENABLED, neatnik.INPUT,  neatnik.IDENTITY, 0, number_of_inputs-i))        

        vertexes_list.append((number_of_inputs,     None, neatnik.ENABLED, neatnik.BIAS,   neatnik.UNITY,    0, 0))
        
        for i in range(len(ares_params)): #adding all the parameters as outputs
            vertexes_list.append((number_of_inputs + 1 + i, None, neatnik.ENABLED, neatnik.OUTPUT, neatnik.LOGISTIC, 1, i))

        self.vertexes = vertexes_list

        edges_list = []
        for i in range(len(ares_params)): #connecting the biasing node to the output nopdes
            edges_list.append((None, None, neatnik.ENABLED, neatnik.BIASING, number_of_inputs, number_of_inputs + 1 + i, None))
        self.edges = edges_list
        
        #loading from pre-generated files
        training_outputs = normalize(np.loadtxt(os.environ["PROJECT"]+"/nis/nn_training_params.gz"))
        training_inputs = call_rbfi_many(rbfilist, training_outputs)

        #using these as the stimulation and responses
        self.stimuli = np.array([training_inputs])
        self.responses = np.array([training_outputs])

        self.gc = 0 #stands for generation count

    def fitness(self, organism: Organism) -> float:
        """ Scores the fitness of the input Organism. """
        reactions = organism.react()

        interp_details = call_rbfi_many(rbfilist, np.asarray(reactions[0]))
	
        avg_differences = np.mean(np.abs(interp_details-self.stimuli[0]), axis = 0)
        #this next line gives a weight to the 3 details. The weights are pretty arbitrary at this point.
        return np.exp(-(1/200*avg_differences[0] + 1/30*avg_differences[1] + 1/20*avg_differences[2]))

    def display(self) -> None:
        """ Displays information about the Experiment on the screen. """
        max_score = experiment.genus.species[neatnik.DOMINANT][0].organisms[neatnik.DOMINANT][0].score 

        print("Max. Fitness:", "%.5f"%max_score, end="\r", flush=True)
        #print("Max fitness", max_score, flush = True)

        display_score[self.gc] = max_score
        self.gc += 1
	
        return

t1 = time.time()
experiment = inv_ares()
experiment.run()

if experiment.MPI_rank == 0:
    t2 = time.time()
    print("Total time difference:", t2-t1)

    #input("\nNEATnik has finished.")

    # print(experiment.genus.species[neatnik.DOMINANT][0].organisms[neatnik.DOMINANT][0].graph())

    max_score = experiment.genus.species[neatnik.DOMINANT][0].organisms[neatnik.DOMINANT][0].score                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            #print("Max. Fitness:", "%.2f"%max_score, end="\r", flush=True)                                                                                                                                                                                                                                                              print("Max fitness", max_score, flush = True)
    print("Max fitness", max_score)
    
    np.savetxt(os.environ["PROJECT"]+"/nis/display_score.gz", display_score)

    organism = experiment.genus.species[neatnik.DOMINANT][0].organisms[neatnik.DOMINANT][0];
    pickle.dump(organism.graph(), open(os.environ["PROJECT"]+'/nis/organism_hpw.p', 'wb'))
