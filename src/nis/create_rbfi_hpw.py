import numpy as np
from curve_details import curve_details
from ares_params import redshifts, normalize
from scipy.interpolate import RBFInterpolator
import os
import pickle

if __name__ == "__main__":
	curves = np.loadtxt(os.environ["PROJECT"]+"/nis/interp_curves.gz")
	cd = curve_details(curves)

	Param = normalize(np.loadtxt(os.environ["PROJECT"]+"/nis/interp_params.gz"))
	
	rbfilist = []
	for i in range(cd.shape[1]):
		rbfilist.append(RBFInterpolator(Param, cd[:,i], kernel = "quintic", neighbors = 100))

	#saving the rbf interpolators
	os.chdir(os.environ["PROJECT"]+"/nis")
	if not "hpw_rbfis" in os.listdir():
		os.mkdir("hpw_rbfis")
	os.chdir("hpw_rbfis")
	for i in range(len(rbfilist)):
		with open("rbfi"+str(i)+".pickle", "wb") as f:
			pickle.dump(rbfilist[i], f)
