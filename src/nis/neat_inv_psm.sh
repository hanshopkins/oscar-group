#loads the modules and adds the neatnik path
module load python/3.9.8
module load intel/2019u4
module load intelmpi/2019u4
export PYTHONPATH="/home/s/sievers/hans1/NEATnik/neatnik/build"

#clears the output folder
rm -rf /project/s/sievers/hans1/nis/outputs/*

#runs the script
mpirun -n 40 python neat_inv_psm.py

cd /project/s/sievers/hans1/nis
#plots the organism
python plot_organism.py
mv -f organism_plot.png outputs/organism_plot.png

#calculates the test points
cd ~/oscar-group/src/nis
python nn_guess_test_ares_params.py
cd ~
. ./use_ares.sh
cd /project/s/sievers/hans1/nis
python plot_test.py
python plot_score.py skip_burnin

#zips the output folder
zip neat_inv_outputs.zip outputs/*
