import numpy as np
import pymp
from call_ares import call_ares
from ares_params import ares_params, redshifts, denormalize
num_params = len(ares_params)
import time
import os

def gen_dataset (N, fname, bigger = False):
	curves_array = pymp.shared.array([N,redshifts.shape[0]])

	#randomly choosing output values
	if bigger == False:
		params_array = denormalize(np.random.rand(N,num_params))
	else:
		params_array = denormalize(np.random.rand(N,num_params)*1.2-0.1)

	with pymp.Parallel(40) as p:
		for index in p.range(0,N):
			input_dict = {}
			for i in range(num_params):
				input_dict[ares_params[i][0]] = params_array[index][i]
			curves_array[index] = call_ares(input_dict, redshifts)
	
	np.savetxt(os.environ["PROJECT"]+"/nis/"+fname+"_curves.gz", curves_array)
	np.savetxt(os.environ["PROJECT"]+"/nis/"+fname+"_params.gz", params_array)

if __name__ == "__main__":
	print("Starting")
	t1 = time.time()
	gen_dataset(4000, "nn_training", False)
	t2 = time.time()
	print("Finished Neatnik training dataset, took",str(t2-t1),"seconds")
	gen_dataset(4000, "interp", True)
	t3 = time.time()
	print("Finished interpolation dataset, took",str(t3-t2),"seconds")
	gen_dataset(4000, "test", False)
	t4 = time.time()
	print("Finished testing dataset, took",str(t4-t3),"seconds")
