import numpy as np
import neatnik
import pickle as p
from ares_params import denormalize
from curve_details import curve_details

graph = p.load(open("/project/s/sievers/hans1/nis/organism_hpw.p", "rb"))

neural_network = neatnik.Organism(graph)

testCurves = np.loadtxt("/project/s/sievers/hans1/nis/test_curves.gz")[:5]
testCD = curve_details(testCurves)

reactions = np.empty([5,2])
for i in range(5):
	reactions = np.asarray(neural_network.react([testCD])[0])

denormalized_reactions = denormalize(reactions)

np.savetxt("/project/s/sievers/hans1/nis/nn_testOP.txt",denormalized_reactions)
