import numpy as np
from ares_params import ares_params, redshifts, normalize
from scipy.interpolate import RBFInterpolator
import time
import pickle
import os

#importing the computed curves
inputs_array = np.loadtxt(os.environ["PROJECT"]+"/nis/interp_curves.gz")
outputs_array = np.loadtxt(os.environ["PROJECT"]+"/nis/interp_params.gz")
normalized_outputs_array = normalize(outputs_array)

N = outputs_array.shape[0]

interp_points = np.empty([N*redshifts.shape[0],len(ares_params)+1])
for i in range(N):
	points_per_curve = np.append(redshifts[np.newaxis].T, np.tile(normalized_outputs_array[i],(redshifts.shape[0],1)), axis = 1) #this line is cursed but it works
	interp_points[i*redshifts.shape[0]:(i+1)*redshifts.shape[0],:] = points_per_curve

interp_values = np.empty(N*redshifts.shape[0])
for i in range(N):
	interp_values[i*redshifts.shape[0]:(i+1)*redshifts.shape[0]] = inputs_array[i]

t1 = time.time()
rbfi = RBFInterpolator(interp_points,interp_values,kernel="inverse_quadratic", epsilon = 1, neighbors = 500)
print("Time difference:",time.time()-t1)

with open(os.environ["PROJECT"]+"/nis/ares_rbfi.pickle", "wb") as f:
	pickle.dump(rbfi,f)

#from __future__ import print_function
