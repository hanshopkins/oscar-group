#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=40
#SBATCH --time=2:00:00
#SBATCH --job-name=generate_ares_dataset
#SBATCH --output=/scratch/s/sievers/hans1/gen_ares_pymp_%j.txt
#SBATCH --mail-user=hans.hopkins@mail.mcgill.ca
#SBATCH --mail-type=ALL
cd $SLURM_SUBMIT_DIR

export ARES=/home/s/sievers/hans1/ares
export PYTHONPATH="${PYTHONPATH}:/home/s/sievers/hans1/ares:/home/s/sievers/hans1/ares/scripts"
module load python/3.7.9
source /home/s/sievers/hans1/.virtualenvs/aresEnv/bin/activate

python gen_ares_pymp.py
