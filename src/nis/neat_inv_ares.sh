#loads the modules and adds the neatnik path
module load python/3.9.8
module load intel/2019u4
module load intelmpi/2019u4
export PYTHONPATH="/home/s/sievers/hans1/NEATnik/neatnik/build"

#clears the output folder
rm -rf $PROJECT/nis/outputs/*

#runs the script
mpirun -n 40 python neat_inv_ares.py
python nn_error.py

cd $PROJECT
#plots the organism
python plot_organism.py
mv -f organism_plot.png outputs/organism_plot.png

#calculates the test points
cd ~/oscar-group/src/nis
python nn_guess_test_ares_params.py
cd ~
. ./use_ares.sh
cd $PROJECT/nis
python plot_test.py
python plot_score.py

#zips the output folder
zip -r neat_inv_outputs.zip outputs/*
