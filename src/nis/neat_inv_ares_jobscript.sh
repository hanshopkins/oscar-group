#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=40
#SBATCH --time=12:00:00
#SBATCH --job-name=neatnik_inv_ares
#SBATCH --output=/scratch/s/sievers/hans1/neatnik_inv_ares_%j.txt
#SBATCH --mail-user=hans.hopkins@mail.mcgill.ca
#SBATCH --mail-type=ALL
cd $SLURM_SUBMIT_DIR

#loads the modules and adds the neatnik path
module load python/3.9.8
module load intel/2019u4
module load intelmpi/2019u4
export PYTHONPATH="/home/s/sievers/hans1/NEATnik/neatnik/build"

#clears the output folder
rm -rf /project/s/sievers/hans1/nis/outputs/*

#runs the script
mpirun -n 40 python neat_inv_ares.py
python nn_error.py

cd /project/s/sievers/hans1/nis
#plots the organism
python plot_organism.py
mv -f organism_plot.png outputs/organism_plot.png

#calculates the test points
cd ~/oscar-group/src/nis
python nn_guess_test_ares_params.py
cd ~
. ./use_ares.sh
cd /project/s/sievers/hans1/nis
python plot_test.py
python plot_score.py

#zips the output folder
zip -r neat_inv_outputs.zip outputs/*
