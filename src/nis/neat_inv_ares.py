#import
import neatnik
import parameters

# Typing:
from neatnik import Experiment
from neatnik import Organism

# Others:
import numpy as np
import pickle
from ares_params import ares_params, redshifts, normalize, denormalize
from call_rbfi_t2 import call_rbfi_many
import time
import os

#loading the rbfi
#with open("/project/s/sievers/hans1/nis/ares_rbfi.pickle", "rb") as file:
#    rbfi = pickle.load(file)

#loading the rbfis
os.chdir(os.environ["PROJECT"]+"/nis/t2_rbfis")
rbfilist = []
for i in range(len(os.listdir())):
	with open("rbfi"+str(i)+".pickle", "rb") as file:
		rbfilist.append(pickle.load(file))

display_score = np.empty(0)

class inv_ares(Experiment):
    """ Find parameters from an ARES sim """

    def __init__(self) -> None:
        """ Initializes experiment """

        super().__init__()

        number_of_inputs = redshifts.shape[0]
        vertexes_list = []
        for i in range(number_of_inputs):
            vertexes_list.append((i, None, neatnik.ENABLED, neatnik.INPUT,  neatnik.IDENTITY, 0, i))
        vertexes_list.append((number_of_inputs,     None, neatnik.ENABLED, neatnik.BIAS,   neatnik.UNITY,    0, 0))
        for i in range(len(ares_params)): #adding all the parameters as outputs
            vertexes_list.append((number_of_inputs + 1 + i, None, neatnik.ENABLED, neatnik.OUTPUT, neatnik.LOGISTIC, 1, 10*i))
        self.vertexes = vertexes_list
        
        edges_list = []
        for i in range(len(ares_params)): #connecting the biasing node to the output nopdes
            edges_list.append((None, None, neatnik.ENABLED, neatnik.BIASING, number_of_inputs, number_of_inputs + 1 + i, None))
        self.edges = edges_list
        
        ##################loading exisitng neural network as base
        #self.vertexes, self.edges = pickle.load(open(os.environ["PROJECT"]+'/nis/organism.p', 'rb'))

        #loading from pre-generated files
        training_inputs = np.loadtxt(os.environ["PROJECT"]+"/nis/ares_training_inputs.gz")
        training_outputs = normalize(np.loadtxt(os.environ["PROJECT"]+"/nis/ares_training_outputs.gz"))

        #using these as the stimulation and responses
        self.stimuli = np.array([training_inputs])
        self.responses = np.array([training_outputs])

        self.gc = 0 #stands for generation count

    def fitness(self, organism: Organism) -> float:
        """ Scores the fitness of the input Organism. """
        reactions = organism.react()

        interp_curves = call_rbfi_many(rbfilist, np.asarray(reactions[0]))

        #absolute differences
        abs_error = np.trapz(np.abs(interp_curves-self.stimuli[0]), redshifts, axis=1)
        
        #relative differences integral
        #rel_error = np.trapz(np.abs(1-interp_curves/self.stimuli[0]), redshifts, axis=1)

        #exponential function
        #return np.sum(np.exp(-differences))/(differences.shape[0]*differences.shape[1])

        #rational function
        return 1.0/(1.0+np.mean(abs_error))

    def display(self) -> None:
        """ Displays information about the Experiment on the screen. """
        max_score = experiment.genus.species[neatnik.DOMINANT][0].organisms[neatnik.DOMINANT][0].score 
        
        print("Max. Fitness:", "%.5f"%max_score, end="\r", flush=True)
        #print("Max fitness", max_score, flush = True)

        global display_score
        display_score = np.append(display_score, max_score)
        self.gc += 1
	
        #saving the organism and score after every generation
        if experiment.MPI_rank == 0:
            np.savetxt(os.environ["PROJECT"]+"/nis/display_score.gz", display_score)
            organism = experiment.genus.species[neatnik.DOMINANT][0].organisms[neatnik.DOMINANT][0];
            pickle.dump(organism.graph(), open(os.environ["PROJECT"]+'/nis/organism.p', 'wb'))

        return

t1 = time.time()
experiment = inv_ares()
experiment.run()

if experiment.MPI_rank == 0:
    t2 = time.time()
    print("Total time difference:", t2-t1)

    #input("\nNEATnik has finished.")

    # print(experiment.genus.species[neatnik.DOMINANT][0].organisms[neatnik.DOMINANT][0].graph())

    max_score = experiment.genus.species[neatnik.DOMINANT][0].organisms[neatnik.DOMINANT][0].score                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            #print("Max. Fitness:", "%.2f"%max_score, end="\r", flush=True)                                                                                                                                                                                                                                                              print("Max fitness", max_score, flush = True)
    print("Max fitness", max_score)
    
    np.savetxt(os.environ["PROJECT"]+"/nis/display_score.gz", display_score)

    organism = experiment.genus.species[neatnik.DOMINANT][0].organisms[neatnik.DOMINANT][0];
    pickle.dump(organism.graph(), open(os.environ["PROJECT"]+'/nis/organism.p', 'wb'))
