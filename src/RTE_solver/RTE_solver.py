import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import CubicSpline

n_H0 = 0.18274212
c = 3E8
H_0 = 70.2*10**3/3.086e22 #s^-1
Omega_m = 1-0.728
rho_b_0 = 2E-13
c_UV = 1.5E38
dfcolldt = 2E9/31557600 # in units of 1/s

ncells = 203
start_z = 5
stop_z = 106.5
cell_spacing = (stop_z-start_z)/ncells
cells = np.linspace(start_z,stop_z,ncells)

ion_start = 30

##exponential model for xe
#x_e = np.linspace(start_z,stop_z,ncells)
#x_e = np.where(x_e < ion_start, 1/5 * np.exp(-x_e/30) - 1/5*np.exp(-1), np.zeros(ncells))

#xe from table
xe_table_raw = np.loadtxt("xe_a.dat")
xe_table_raw[:,0] = 1/xe_table_raw[:,0]-1 #transforming a-values to z-values
xe_spline = CubicSpline(xe_table_raw[-1300::-1,0], xe_table_raw[-1300::-1,1])
xe_table = xe_spline(cells)
#plt.plot(cells, xe_table)

#epsilon for ion
epsilon_nu = np.linspace(start_z,stop_z,ncells)
epsilon_nu = np.where(xe_table < ion_start, 4000*np.ones(ncells), np.zeros(ncells))


############################## sigma function
y_0= 0
y_1= 0
E_0= 0.4298
P=2.963
y_w=0
y_a=32.88
sigma_0= 54750
def sigma(nu):
    E = nu * 4.1357 * 10**-15
    x= E/E_0 - y_0
    y=np.sqrt(x**2 + y_1**2)
    F = (((x-1)**2+y_w**2)*y**(0.5*P-5.5))*(1+np.sqrt(y/y_a))**(-P)
    return sigma_0 * F*10**(-18)/(10**4)
##############################

#step 1: get the optical depth
def tau (bin_num, nu):
    z = start_z+bin_num*(stop_z-start_z)/ncells
    zprime = start_z+(bin_num+1)*(stop_z-start_z)/ncells
    return 2/3*sigma(nu) * c * n_H0/(H_0 * np.sqrt(Omega_m)) * (1-xe_table[bin_num]) * ((zprime+1)**(3/2)-(z+1)**(3/2))

def H (z):
    return H_0 * np.sqrt(Omega_m *(1+z)**3)

if __name__ == "__main__":
    current_nu = 17*4.1357E15
    taus = np.empty(ncells)
    for i in range(ncells):
        taus[i] = tau(i, current_nu)
    
    epsilon_136 = rho_b_0 * c_UV * dfcolldt
    
    J_integral = np.sum(np.exp(-taus)/H(cells))*cell_spacing
    print("tau:", taus[0], "H:", H(cells[0]), "cell_spacing:", cell_spacing)
    print(np.exp(-taus[0])/H(cells[0])*cell_spacing)
    print(J_integral)
    def J (z):
        return c/(4*np.pi)*(1+z)**2*epsilon_136*J_integral
    
    plt.plot(cells, taus)
    plt.xlabel("z")
    plt.ylabel(r"$\tau_\nu$")
    plt.title(r"Optical depth values for $\nu$ = "+str(current_nu)+r" s$^{-1}$")
    plt.savefig("optical_depth.png")
    
    fig1 = plt.figure()
    plt.plot(cells, J(cells))
    plt.xlabel("z")
    plt.ylabel(r"$J_\nu(z)$")
    plt.title(r"Solved J integral for $\nu$ = "+str(current_nu)+r" s$^{-1}$")
    plt.savefig("J_integral.png")