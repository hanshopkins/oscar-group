import numpy as np
import matplotlib.pyplot as plt
import pickle
import sys
import os
import time
sys.path.append("/home/s/sievers/hans1/oscar-group/src/nis")
from call_rbfi_t2 import call_rbfi_many
from ares_params import redshifts, normalize
from curve_details import curve_details

testCurves = np.loadtxt("ares_test_curves.gz")
testParams = np.loadtxt("ares_test_params.gz")
normalizedTestparams = normalize(testParams)

os.chdir("/project/s/sievers/hans1/nis/t2_rbfis")
rbfilist = []
for i in range(len(os.listdir())):
	with open("rbfi"+str(i)+".pickle", "rb") as file:
		rbfilist.append(pickle.load(file))


if __name__ == "__main__":
	t1 = time.time()
	rbfiCurves = call_rbfi_many(rbfilist, normalizedTestparams)
	print("Average time to get curve from interpolator:", (time.time()-t1)/5)

	plt.plot(redshifts, testCurves[0], color="r", ls=":", label="true curve")
	plt.plot(redshifts, testCurves[1], color="g", ls=":")
	plt.plot(redshifts, testCurves[2], color="b", ls=":")
	plt.plot(redshifts, testCurves[3], color="c", ls=":")
	plt.plot(redshifts, testCurves[4], color="m", ls=":")

	plt.plot(redshifts, rbfiCurves[0], color="r", ls="-", label = "rbfi approx.")
	plt.plot(redshifts, rbfiCurves[1], color="g", ls="-")
	plt.plot(redshifts, rbfiCurves[2], color="b", ls="-")
	plt.plot(redshifts, rbfiCurves[3], color="c", ls="-")
	plt.plot(redshifts, rbfiCurves[4], color="m", ls="-")
	plt.xlabel(r"$z$")
	plt.ylabel(r"$\delta T_b$ (mK)")
	plt.title("Testing one-per-rs-point rbfi")
	plt.legend()
	os.chdir("..")
	plt.savefig("outputs/rbfi_t2_testplot")

print("Maximum difference:", np.max(np.abs(testCurves-rbfiCurves)))
print("Average difference:", np.mean(np.abs(testCurves-rbfiCurves)))
