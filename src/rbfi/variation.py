import numpy as np
from rbfi_lib import *
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def quad(x, a, b, c):
    return (a * x**2) + (b * x) + c

def lin(x, a, b):
    return (a * x) + b

def exp(x, a, b):
    return a * np.exp (x * b)

file = np.loadtxt(datasets + "points_variation.txt")

points = file.T[0]
acc = file.T[1]
times = file.T[2]

quad_fit = curve_fit(quad, points, times)
lin_fit = curve_fit(lin, points, 1 / acc)
print(np.sum(lin(points, lin_fit[0][0], lin_fit[0][1]) - (1 / acc)))


plt.plot(points, 1 / acc)
plt.xlabel("number of points")
plt.ylabel("accuracy")
# plt.show()
plt.close()

plt.plot(points, times)
plt.xlabel("number of points")
plt.ylabel("computation time in s")
# plt.show()