import numpy as np
from rbfi_lib import *
from sklearn.utils import shuffle
import matplotlib.pyplot as plt
z_domain = np.linspace(5, 30, 40)
p, t, P, T = process_par_temp_data("aug8_trainset_params.gz", "aug8_trainset_curves.gz", "aug8_testset_params.gz", "aug8_testset_curves.gz", z_domain)
p_n = normalize(p)
P_n = normalize(P)
kernels = ["linear", "thin_plate_spline", "cubic", "quintic", "multiquadric"]

p_n, t = shuffle(p_n, t, random_state=0)
P_n, T = shuffle(P_n, T, random_state=0)
errs = []
times = []
for n in range(100, p.shape[0], int(p.shape[0] / 100)):      
    try:
        n = int(n)
        name, this_time = make_rbfi(p_n[:n], t[:n], kernel='quintic')

        with open(models + name, "rb") as f:
            rbfi = pickle.load(f)

        err = error(rbfi, P_n[:n], T[:n])
        errs.append(err)
        times.append(this_time)
        print(n, err, this_time)
    except:
        break


<<<<<<< HEAD

neighbours = np.linspace(6, 100, 10)
#epsilons = np.linspace(1, 10, 10)
#degrees = np.linspace(1,10,10)
kernels = [
    "linear",
    "thin_plate_spline",
    "cubic",
    "quintic"
    ]
kernels_e = [
    "multiquadric",
    "inverse_multiquadric",
    "inverse_quadratic",
    "gaussian"
    ]
rbfi_list = []
kernels_tot = [

    "linear",
    "thin_plate_spline",
    "cubic",
    "quintic",
    "multiquadric",
    "inverse_multiquadric",
    "inverse_quadratic",
    "gaussian"
        ]
bad_values = []
for k in range(len(kernels_tot)):
    for n in neighbours:
        if k < 4:
            epsilons =[ 1]
        else:
            epsilons = np.linspace(1, 10, 10)

        for e in epsilons:
            
            
            if k == 4:
                minimum = 0
            elif k == 0:
                minimum = 0
            elif k == 1:
                minimum = 1
            elif k == 2:
                minimum = 1
            elif k == 3:
                minimum = 2
            else:
                minimum = 0
            degrees = np.linspace(minimum, 10, 11-minimum)
            for d in degrees:
                if d == 1 and n <7:
                    continue

                elif d ==2 and n <28:
                    continue
                elif d == 3 and n <84:
                    continue
                elif d>= 4 and n <126:
                    continue
                rbfi = make_rbfi(p_n, t, neighbors = n, kernel = kernels_tot[k], degree = d, epsilon = e)
                name = rbfi[0]
                time = rbfi[1]
                with open(models + name, "rb") as f:
                    rbfi = pickle.load(f)
                print(kernels_tot[k], n, e,d)

                try:
                    rbfi_list.append([kernels_tot[k], n, e,d, time, error(rbfi, P_n, T)])
                except:
                    bad_values.append([kernels_tot[k], n, e,d, time])
=======
with open(datasets + "err_points.p", "rb") as f:
    pickle.dump(errs, f)

with open(datasets + "time_points.p", "rb") as f:
    pickle.dump(times, f)
>>>>>>> 14cc22f (points effectiveness variation measured)

# with open(datasets + "err_points.p", "rb") as f:
#     errs = pickle.load(f)

plt.plot(np.linspace(0, len(errs), len(errs)), errs)
plt.show()

<<<<<<< HEAD
#data = np.loadtxt("text.txt")
#print(rbfi_list)
rbfi_list = pd.DataFrame(rbfi_list)
bad_values = pd.DataFrame(bad_values)
print(rbfi_list)

rbfi_list.to_csv('test_2_rbfi_list.txt', header=None, index=None, sep=' ', mode='a')
bad_values.to_csv('test_2_bad_values', header=None, index=None, sep=' ', mode='a')
#np.savetxt("test_1_rbfi_list.txt", rbfi_list)
#np.savetxt("test_1_bad_values.txt",bad_values)
'''
for k in kernels_tot:
    plt.figure()
    values = rbfi_list.loc[rbfi_list[:][0] == k]
    for d in range(1,5):
        values_d = values.loc[values[:][3] == d]
        for e in epsilons:
            values_e = values_d.loc[values_d[:][2] == e]
            plt.plot(values_e.loc[:][1],values_e.loc[:][5], label = k + " d =" +str(d)+ " e =" +str(e))
    plt.legend()
    plt.savefig(str(k) + "test_1.png")
'''
=======
plt.plot(np.linspace(0, len(times), len(times)), times)
plt.show()
>>>>>>> 14cc22f (points effectiveness variation measured)
