# RBFI
## Summary

This section contains tools and scripts pertaining to the use of radial basis function interpolation ([RBFI](https://en.wikipedia.org/wiki/Radial_basis_function_interpolation)).

### [rbfi_lib.py](https://gitlab.com/hanshopkins/oscar-group/-/tree/main/src/rbfi/rbfi_lib.py) contains many of the useful functions used by the scripts in the directory:

- process_par_temp_data:   

Transforms gz compressing training and testing data into usable arrays that consider redshifts as parameters.

    Parameters
    ----------
    p_train_file: string
        File name of the training parameters.
    t_train_file: string
        File name of the training temperatures.
    p_test_file: string
        File name of the testing parameters.
    t_test_file: string
        File name of the testing temperatures.
    z_domain: numpy array
        The range of redshift values to be used.

    Returns
    -------
    result: array_like
     
- normalize

Normalizes a data set of ares parameters.

    Parameters
    ----------
    arr: numpy array
        List of points to be normalized.

    Returns
    -------
    op: numpy array
        Normalized list of parameters.

 - make_rbfi

 Fits an RBF to some data, RBFInterpolators optional arguments can also be supplied

    Paramters
    ---------
    y: array like
        The interpolation points.
    d: array like
        The interpolation points' values.

    Returns
    -------
    time_rbfi: float
        The time it took to complete the RBFI creation

- error

Gives the mean percent error of a function on predicting the values of a dataset.

    Parameters
    ----------
    function: function
        The function whose error is quantified.
    Y: array like
        The testing points.
    D: array like
        The testin points' values.

    Returns
    -------
    None

- call_ares

 Returns the temperature fluctuation vs redshift curve.

    Parameters
    ----------
    ares_params: dictionary
        Specify which the values of the desired paramamters.
    redshift: numpy array
        Specify the redshift over which to graph the curve.
    
    Returns
    -------
    redshifts: cubic spline
        A cubic spline of the temperature fluctuation vs redshift curve produced by ares.
    
