import os
import time
import numpy as np
from scipy.interpolate import RBFInterpolator
from scipy.interpolate import CubicSpline
import pickle
import ares
import matplotlib.pyplot as plt


# Important variables for rbfi related tasks
home = os.getenv("HOME")
models = home + "/oscar-group/data/rbfi/models/"
datasets = home + "/oscar-group/data/rbfi/datasets/"
plots = home + "/oscar-group/data/rbfi/plots/"
ares_params = [
<<<<<<< HEAD
        ["pop_Tmin_0_", 1E3, 1E5, "log"],
        ["pop_fesc_0_", 0, 1, "lin"],
        ["pop_rad_yield_1_", 5E39, 5E40, "log"],
        ["pq_func_par2[0]_0_", 0.1, 1, "lin"], #gamma_lo
        ["pq_func_par0[0]_0_", 0.001, 0.1, "log"], #f_star,p
        ["redshifts", 5, 30, "lin"]
        ]

'''
[
	["pop_rad_yield_0_", 1E3, 1E6, "log"],
	["pop_rad_yield_1_", 1E37, 5E40, "log"],
	["pop_rad_yield_2_", 1E3, 5E6, "log"],
	["clumping_factor", 0.05, 2, "lin"],
    ["redshifts", 5, 30, "lin"]
]
'''
=======
    ["pop_Tmin_0_", 1E3, 1E5, "log"],
    ["pop_fesc_0_", 0, 1, "lin"],
    ["pop_rad_yield_1_", 5E39, 5E40, "log"],
    ["pq_func_par2[0]_0_", 0.1, 1, "lin"], #gamma_lo
    ["pq_func_par0[0]_0_", 0.001, 0.1, "log"],#f_star,p
    ["redshift", 5, 30, "lin"]
] 


>>>>>>> 14cc22f (points effectiveness variation measured)

def process_par_temp_data(p_train_file, t_train_file, p_test_file, t_test_file, z_domain):
    '''
    Transforms gz compressing training and testing data into usable arrays that consider redshifts as parameters.

    Parameters
    ----------
    p_train_file: string
        File name of the training parameters.
    t_train_file: string
        File name of the training temperatures.
    p_test_file: string
        File name of the testing parameters.
    t_test_file: string
        File name of the testing temperatures.
    z_domain: numpy array
        The range of redshift values to be used.

    Returns
    -------
    result: array_like
        Returns a list of numpy arrays. The first array contains all points to be evaluated using redshift as a parameter for the training set, the third does the same for the testing set. The second and fourth are the corresponding temperatures.
    '''
    # Loading the data from files.
    p_train = np.loadtxt(datasets + p_train_file)
    t_train = np.loadtxt(datasets + t_train_file)
    p_test = np.loadtxt(datasets + p_test_file)
    t_test = np.loadtxt(datasets + t_test_file)

    z_size = z_domain.shape[0]
    result = []
    for d in [p_train, t_train], [p_test, t_test]:
        # Remove zeroes
        zeros = np.where(d[0].T[0] == 0)
        t = d[1][d[1] != np.zeros(z_size)]
        p = np.delete(d[0], zeros[0], axis = 0)

        # Making redshift a parameter
        z_domain_tile = np.tile(z_domain, p.shape[0])
        p = np.repeat(p, z_size, axis = 0)
        points = np.concatenate((p.T, np.array([z_domain_tile])), axis = 0).T
        result.append(points)
        result.append(t)

    return result


def normalize(arr):
    '''
    Normalizes a data set of ares parameters.

    Parameters
    ----------
    arr: numpy array
        List of points to be normalized.

    Returns
    -------
    op: numpy array
        Normalized list of parameters.
    '''
    op = np.empty(arr.shape)
    for i in range(arr.shape[1]):
        
        if ares_params[i][3] == "lin":
            op[:,i] = (arr[:,i] - ares_params[i][1])/(ares_params[i][2] - ares_params[i][1])
        elif ares_params[i][3] == "log":
            op[:,i] = (np.log10(arr[:,i]/ares_params[i][1]))/(np.log10(ares_params[i][2]/ares_params[i][1]))
        else:
            raise ValueError("Invalid normalization type in ares_params")
    return op


def make_rbfi(y, d, neighbors = None, kernel = 'thin_plate_spline', epsilon = None, degree = None):
    '''
    Fits an RBF to some data, RBFInterpolators optional arguments can also be supplied

    Paramters
    ---------
    y: array like
        The interpolation points.
    d: array like
        The interpolation points' values.

    Returns
    -------
    time_rbfi: float
        The time it took to complete the RBFI creation
    '''
    time_start = time.time()
    rbfi = RBFInterpolator(y, d, neighbors, kernel = kernel, epsilon = epsilon, degree = degree)
    time_rbfi = time.time() - time_start
    delim = '-'
    name = kernel + delim + str(neighbors) + delim + str(epsilon) + delim + str(degree) + delim + "rbfi.p"
    with open(models + name, "wb") as f:
        pickle.dump(rbfi,f)
    return name, time_rbfi


def error(function, Y, D):
    '''
    Gives the mean percent error of a function on predicting the values of a dataset.

    Parameters
    ----------
    function: function
        The function whose error is quantified.
    Y: array like
        The testing points.
    D: array like
        The testin points' values.
    '''
    err_abs = D - function(Y)
    err = np.abs(err_abs / D)
    return np.mean(err)


def call_ares(ares_params, redshifts):
    '''
    Returns the temperature fluctuation vs redshift curve.

    Parameters
    ----------
    ares_params: dictionary
        Specify which the values of the desired paramamters.
    redshift: numpy array
        Specify the redshift over which to graph the curve.
    
    Returns
    -------
    redshifts: cubic spline
        A cubic spline of the temperature fluctuation vs redshift curve produced by ares.
    '''
    sim = ares.simulations.Global21cm(**ares_params, verbose=False, progress_bar=False)
    sim.run()
    z = sim.history['z'][::-1]
    dTb = sim.history['dTb'][::-1]
    sorted_idx = np.argsort(z,kind="stable")
    z = z[sorted_idx]
    dTb = dTb[sorted_idx]
    spline = CubicSpline(z, dTb)	
    return spline(redshifts)
