import numpy as np
import ares
from scipy.interpolate import CubicSpline

def call_ares (ares_params, redshifts):
	if False: #default
		sim = ares.simulations.Global21cm(**ares_params, verbose=False, progress_bar=False)
	else: #realistic galaxy params
		pbundle = ares.util.ParameterBundle('mirocha2017:base')
		pbundle.update(ares_params)
		pbundle.update({"pop_binaries_0_":True, "pop_nebular_0_": True})
		sim = ares.simulations.Global21cm(**pbundle, verbose=False, progress_bar=False)
	sim.run()
	z = sim.history['z'][::-1]
	dTb = sim.history['dTb'][::-1]
	sorted_idx = np.argsort(z,kind="stable")
	z = z[sorted_idx]
	dTb = dTb[sorted_idx]
	spline = CubicSpline(z, dTb)
	return spline(redshifts)
