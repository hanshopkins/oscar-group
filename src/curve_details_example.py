import numpy as np
import curve_details as cd

curves = np.loadtxt('../data/example_data/fstar_fX_example_curves.gz')
output = cd.curve_details(curves)
print('The output of curve_details is a [#curves, 3] numpy array. The first column contains the height of each trough. The second column contains the position of each trough. The third column contains the width of each trough.')
print(output)
